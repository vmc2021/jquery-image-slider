//use strict
var slideIndex = 0;
// Next/previous controls
$('.next').click(function() {
    slideIndex++;
    showSlides();
})

$('.prev').click(function() {
    slideIndex--;
    showSlides()
})


function showSlides() {
    // Grab all slides from page
    var slides = $('.picture_container');

    // If at the end, loop back to the beginning
    if (slideIndex == slides.length) {slideIndex = 0}
    // If at the beginning, loop back to the end
    if (slideIndex < 0) {slideIndex = slides.length - 1}

    // Go ahead and hide all of our slides
    slides.addClass('hide');
    $('.img').addClass('invis');

    // Loop through the slides and show the one that we need
    slides.each(function(index) {
        // If the slide is the one we need, unhide it
        if (slideIndex == index) {
            $(this).removeClass('hide');
            // Have the image fade in after 100ms
            setTimeout(() => {
                $(this).children('.img').removeClass('invis');
            }, 100)

        }
    })
}

// Show/hide the description for the image
$('.name_container').click(function() {
    // If the description is currently showing, hide it
    if ($(this).hasClass('active')) {
        // Animate back to original height of 50px
        $(this).animate({height: "50px"}, 500)
        $(this).removeClass('active');
    } else {
        // If the description is not showing, animate the height so that it is tall enough to show our description
        $(this).animate({height: "75px"}, 500)
        $(this).addClass('active');
    }
})



